# Dell XPS Tweaks taken from https://github.com/JackHack96/dell-xps-9570-ubuntu-respin/blob/master/xps-tweaks.sh

# Install power management tools...
sudo add-apt-repository -y ppa:linrunner/tlp
sudo apt -y update
sudo apt -y install thermald tlp tlp-rdw powertop

# Fix Sleep/Wake Bluetooth Bug...
sudo sed -i '/RESTORE_DEVICE_STATE_ON_STARTUP/s/=.*/=1/' /etc/default/tlp
sudo systemctl restart tlp

# Enable LDAC, APTX, APTX-HD, AAC support in PulseAudio Bluetooth...
sudo add-apt-repository ppa:eh5/pulseaudio-a2dp
sudo apt-get update
sudo apt-get install libavcodec-dev libldac pulseaudio-module-bluetooth

# Other packages...
sudo apt -y install intel-microcode

# Install wifi drivers...
sudo rm -rf /lib/firmware/ath10k/QCA6174/hw3.0/*
sudo wget -O /lib/firmware/ath10k/QCA6174/hw3.0/board.bin https://github.com/kvalo/ath10k-firmware/blob/master/QCA6174/hw3.0/board.bin?raw=true
sudo wget -O /lib/firmware/ath10k/QCA6174/hw3.0/board-2.bin https://github.com/kvalo/ath10k-firmware/blob/master/QCA6174/hw3.0/board-2.bin?raw=true
sudo wget -O /lib/firmware/ath10k/QCA6174/hw3.0/firmware-4.bin https://github.com/kvalo/ath10k-firmware/blob/master/QCA6174/hw3.0/firmware-4.bin_WLAN.RM.2.0-00180-QCARMSWPZ-1?raw=true

# Enable power saving tweaks...
sudo echo "options i915 enable_fbc=1 enable_guc=3 disable_power_well=0 fastboot=1" > /etc/modprobe.d/i915.conf

# Check fan speed with lm-sensors...
echo "options dell-smm-hwmon restricted=0 force=1" > /etc/modprobe.d/dell-smm-hwmon.conf
if cat /etc/modules | grep "dell-smm-hwmon" &>/dev/null
then
    sudo echo "dell-smm-hwmon is already in /etc/modules!"
else
    sudo echo "dell-smm-hwmon" >> /etc/modules
fi
sudo update-initramfs -u

# Disable gnome tracker...
gsettings set org.freedesktop.Tracker.Miner.Files enable-monitors false
gsettings set org.freedesktop.Tracker.Miner.Files crawling-interval -2
