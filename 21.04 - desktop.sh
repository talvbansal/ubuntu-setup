#!/bin/sh
sudo apt update && sudo apt upgrade -y

sudo apt install apt-transport-https curl git

# Install Peek PPA
# sudo add-apt-repository ppa:peek-developers/stable

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list


# Docker PPA
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install base dependencies
sudo apt update
sudo apt install -y git brave-browser supervisor unattended-upgrades fail2ban kazam sshuttle tmux unzip software-properties-common wget nodejs libpng-dev ssl-cert git-core vlc ubuntu-restricted-extras geany chromium-browser curl zsh zsh-common imagemagick ntp tmux curl exfat-fuse exfat-utils cmake g++ libboost-all-dev libcurl4-openssl-dev libssl-dev libjansson-dev automake autotools-dev build-essential apt-transport-https docker-ce proj-bin gimp resolvconf peek

# Add current user to webserver group
sudo usermod -aG www-data $USER

# post docker install
sudo groupadd docker
sudo usermod -aG docker $USER

# Get ohmyzsh
chsh -s `which zsh`
curl -L http://install.ohmyz.sh | sh
cd ~/.oh-my-zsh && git clone git://github.com/zsh-users/zsh-syntax-highlighting.git
cd ~/


# Set inital git config
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

# Install snaps
sudo snap install insomnia
sudo snap install slack --classic
sudo snap install phpstorm --classic
sudo snap install pycharm-community --classic
sudo snap install code --classic
sudo snap install storage-explorer
sudo snap install mysql-workbench-community

sudo snap connect storage-explorer:password-manager-service :password-manager-service
sudo snap connect mysql-workbench-community:password-manager-service :password-manager-service
sudo snap connect mysql-workbench-community:ssh-keys

# Configure Node
npm config set prefix ~/.npm
echo 'export PATH=~/.npm/bin:$PATH' >> ~/.profile
source ~/.profile

# Increase node filewatchers limit - needed for expo
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

# Install Composer for php
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Add node and composer to path
source ~/.zshrc
sed -i 's/export PATH=\$HOME\/bin\:\/usr\/local\/bin\:\$PATH/export PATH=\"\$HOME\/.composer\/vendor\/bin\:\$HOME\/.npm\/bin\:\$PATH\"/g' ~/.zshrc
source ~/.zshrc


# Install global PHP and Node packages
composer global require laravel/installer

npm install -g expo-cli

# Install AWS CLI
cd ~/
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install


cd ~/Downloads
wget -c https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb
rm zoom_amd64.deb

curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
rm session-manager-plugin.deb


wget -c https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.25-1ubuntu21.04_amd64.deb
sudo dpkg -i mysql-workbench-community_8.0.25-1ubuntu21.04_amd64.deb
rm mysql-workbench-community_8.0.25-1ubuntu21.04_amd64.deb
cd ~/

# Create a new mysql_native_password MySQL user
#echo "-----------------------------------------------------------"
#echo "| Create a new MySQL user,                                |"
#echo "| run the following commands in the MySQL prompt          |"
#echo "| remember to change the password value                   |"
#echo "---------------------------------------- ------------------"
#echo "GRANT ALL ON *.* TO 'admin'@'%'"
#echo "CREATE USER 'admin'@'%' IDENTIFIED BY 'password';"
#echo "GRANT ALL ON *.* TO 'admin'@'%';"
#echo "FLUSH PRIVILEGES;"
#echo "-----------------------------------------------------------"
#sudo mysql

# Install docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


sudo systemctl enable resolvconf
sudo systemctl start resolvconf

sudo systemctl stop docker
sudo cp daemon.json /etc/docker/daemon.json
sudo systemctl start docker
sudo docker info | grep -i root
