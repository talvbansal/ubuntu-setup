#!/bin/sh
sudo apt update && sudo apt upgrade -y

sudo apt install curl

# PHP PPA
sudo add-apt-repository ppa:ondrej/php -y

# Node JS / Yarn PPA
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# Docker PPA
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install base dependencies
sudo apt update
sudo apt install -y supervisor unattended-upgrades fail2ban nginx php php-zip php-bcmath php-fpm php-mysql php-xml php-sqlite3 php-xdebug php-soap php-curl php-redis php-mbstring php-gd redis-server redis tmux unzip software-properties-common wget nodejs yarn libpng-dev ssl-cert git-core vlc ubuntu-restricted-extras geany git chromium-browser mysql-server mysql-workbench curl zsh zsh-common imagemagick nginx ntp tmux curl exfat-fuse exfat-utils cmake g++ libboost-all-dev libcurl4-openssl-dev libssl-dev libjansson-dev automake autotools-dev build-essential apt-transport-https docker-ce

# Add current user to webserver group
sudo usermod -a -G www-data "$USER"

# Get ohmyzsh
chsh -s `which zsh`
curl -L http://install.ohmyz.sh | sh
cd ~/.oh-my-zsh && git clone git://github.com/zsh-users/zsh-syntax-highlighting.git
cd ~/

# Generate a self signed certificate
sudo make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/ssl/certs/selfsigned.pem

# Set inital git config
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

# Install Gitlab Runner
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# Install snaps
sudo snap install postman
sudo snap install slack --classic
sudo snap install phpstorm --classic
sudo snap install code --classic

# Configure Node
npm config set prefix ~/.npm
echo 'export PATH=~/.npm/bin:$PATH' >> ~/.profile
source ~/.profile

# Increase node filewatchers limit - needed for expo
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

# Install Composer for php
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Add node and composer to path
source ~/.zshrc
sed -i 's/export PATH=\$HOME\/bin\:\/usr\/local\/bin\:\$PATH/export PATH=\"\$HOME\/.composer\/vendor\/bin\:\$HOME\/.npm\/bin\:\$PATH\"/g' ~/.zshrc
source ~/.zshrc


# Install global PHP and Node packages
composer global require laravel/installer

npm install -g expo-cli

cd ~/Downloads
wget -c https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.15-1ubuntu18.10_amd64.deb
sudo dpkg -i mysql-workbench-community_8.0.15-1ubuntu18.10_amd64.deb
rm mysql-workbench-community_8.0.15-1ubuntu18.10_amd64.deb
cd ~/

# Create a new mysql_native_password MySQL user
echo "-----------------------------------------------------------"
echo "| Create a new MySQL user,                                |"
echo "| run the following commands in the MySQL prompt          |"
echo "| remember to change the password value                   |"
echo "---------------------------------------- ------------------"
echo "GRANT ALL ON *.* TO 'admin'@'%' IDENTIFIED BY 'mypassword';"
echo "FLUSH PRIVILEGES;"
echo "-----------------------------------------------------------"
sudo mysql
